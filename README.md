# skedulo-api

Dockerized Spring boot 2.0, h2 in memory database, published on GCP/GKE

# to build and deploy (gcloud suite must be installed and configured to project-id)
./gradlew build docker
docker build . -t gcr.io/{{project-id}}/homework:v1 
gcloud docker -- push gcr.io/{{project-id}}/homework:v1
kubectl run {{deplloyment-name}} --image gcr.io/{{project-id}}/homework:v1 --port=8080
build the loadbalancer service to expose to the internet kubectl expose deployment homework-app --type=LoadBalancer