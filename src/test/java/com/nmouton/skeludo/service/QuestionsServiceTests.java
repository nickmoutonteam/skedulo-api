package com.nmouton.skeludo.service;

import com.nmouton.skeludo.app.Application;
import com.nmouton.skeludo.entities.OrderProduct;
import com.nmouton.skeludo.entities.Performance;
import com.nmouton.skeludo.entities.TimeSlot;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.*;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

@RunWith(SpringRunner.class)
@EnableAutoConfiguration
@SpringBootTest(classes={Application.class})
public class QuestionsServiceTests {

    @Autowired
    QuestionsService questionsService;

    @Test
    public void testQuestion1ShouldReturn100Results(){
        List<String> strings = questionsService.runQuestion1();
        assertEquals(100, strings.size());
    }

    @Test
    public void testQuestion2ShouldJoinNamePrice(){
        OrderProduct orderProduct = OrderProduct.create().setOrderId(1).setProductId(1);
        OrderProduct orderProduct2 = OrderProduct.create().setOrderId(2).setProductId(2);
        List<OrderProduct> orderProducts = Arrays.asList(orderProduct,orderProduct2);

        List<OrderProduct> joinedOrderProducts = questionsService.runQuestion2(orderProducts);
        joinedOrderProducts.sort(Comparator.comparing(OrderProduct::getOrderId));
        assertEquals("Some Other Product", joinedOrderProducts.get(1).getName());
        assertEquals(Double.valueOf(3.5), joinedOrderProducts.get(1).getPrice());
    }

    @Test
    public void bonusQuestionShouldCreateSchedule(){

        LocalDate today = LocalDate.now(Clock.system(ZoneId.of("Australia/Sydney"))).plusDays(1);
        LocalTime startingTime = LocalTime.of(8, 00);
        LocalDateTime tomorrowAtEight = LocalDateTime.of(today,startingTime);

        //band 1 is 10 priority plays from 8 till 9
        Performance performance1 = Performance.create()
                .setBand("band1")
                .setPriority(10)
                .setStartTime(tomorrowAtEight)
                .setFinishTime(tomorrowAtEight.plusHours(1));
        //band 2 is 6 priority plays from 8 till 9
        Performance performance2 = Performance.create()
                .setBand("band2")
                .setPriority(6)
                .setStartTime(tomorrowAtEight.plusHours(1))
                .setFinishTime(tomorrowAtEight.plusHours(2));
        //band 3 is 8 priority plays from 10 till 11
        Performance performance3 = Performance.create()
                .setBand("band3")
                .setPriority(8)
                .setStartTime(tomorrowAtEight.plusHours(2))
                .setFinishTime(tomorrowAtEight.plusHours(3));
        //band 4 is 7 priority plays from 8:15 till 9:15
        Performance performanceClashesWith1And2 = Performance.create()
                .setBand("band4")
                .setPriority(7)
                .setStartTime(tomorrowAtEight.minusMinutes(15))
                .setFinishTime(tomorrowAtEight.plusHours(1).plusMinutes(15));
        //band 5 is 2 priority plays from 8 till 11
        Performance performanceClashesWithEverything = Performance.create()
                .setBand("band5")
                .setPriority(7)
                .setStartTime(tomorrowAtEight.minusMinutes(15))
                .setFinishTime(tomorrowAtEight.plusHours(1).plusMinutes(15));
        //list of bands
        List<Performance> performances = Arrays.asList(performance1,
                performance2,
                performance3,
                performanceClashesWith1And2,
                performanceClashesWithEverything);

        System.out.println(questionsService.runBonusQuestion(performances));
        List<TimeSlot> schedule = questionsService.runBonusQuestion(performances);

        //band1 should be overwrite band4
        Optional<TimeSlot> findEightThirtyTimeSlot = schedule
                .stream()
                .filter(timeSlot -> TimeSlot.create()
                        .setWindow(tomorrowAtEight.plusMinutes(30))
                        .setBand("band1").toString().equals(timeSlot.toString()))
                .findFirst();
        assertTrue(findEightThirtyTimeSlot.isPresent());

        //band4 should overwrite band 2 from 9:01am till 9:15
        Optional<TimeSlot> findNineTimeSlot = schedule
                .stream()
                .filter(timeSlot -> TimeSlot.create()
                        .setWindow(tomorrowAtEight.plusHours(1).plusMinutes(1))
                        .setBand("band4").toString().equals(timeSlot.toString()))
                .findFirst();
        assertTrue(findNineTimeSlot.isPresent());

        //band2 should overwrite band 2 from 9:16am
        Optional<TimeSlot> findNineFifteenTimeSlot = schedule
                .stream()
                .filter(timeSlot -> TimeSlot.create()
                        .setWindow(tomorrowAtEight.plusHours(1).plusMinutes(16))
                        .setBand("band2").toString().equals(timeSlot.toString()))
                .findFirst();
        assertTrue(findNineTimeSlot.isPresent());

        Optional<TimeSlot> thisBandIsNoGood = schedule
                .stream()
                .filter(timeSlot -> "band5".equals(timeSlot.getBand()))
                .findFirst();
        assertFalse(thisBandIsNoGood.isPresent());

    }
}
