package com.nmouton.skeludo.repository;

import com.nmouton.skeludo.entities.OrderProduct;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OrderProductRepository extends CrudRepository<OrderProduct, Long> {

    /**
     * Find all Orders given an order ID with product details cross referenced from the Produc table.
     * This method will be translated into a query by constructing it directly
     * from the method name as there is no other query declared.
     *
     * @param orderIds
     * @return List<OrderProduct>
     */
    List<OrderProduct> findByOrderIdIn(List<Integer> orderIds);

}