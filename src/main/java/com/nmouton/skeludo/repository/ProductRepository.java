package com.nmouton.skeludo.repository;

import com.nmouton.skeludo.entities.Product;

import java.util.List;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductRepository extends CrudRepository<Product, Long> {

    List<Product> findByProductIdIn(List<Integer> productIds);
}
