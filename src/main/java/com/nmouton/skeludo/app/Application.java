package com.nmouton.skeludo.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication(scanBasePackages = {"com.nmouton.skeludo"})
@EnableTransactionManagement
@EnableJpaRepositories(basePackages="com.nmouton.skeludo")
@EnableAutoConfiguration
@EntityScan(basePackages="com.nmouton.skeludo")
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

}
