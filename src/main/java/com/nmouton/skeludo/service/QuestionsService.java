package com.nmouton.skeludo.service;

import com.nmouton.skeludo.entities.OrderProduct;
import com.nmouton.skeludo.entities.Performance;
import com.nmouton.skeludo.entities.Product;
import com.nmouton.skeludo.entities.TimeSlot;
import com.nmouton.skeludo.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.*;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Service
public class QuestionsService {

    @Autowired
    ProductRepository productRepository;

    //Fairly simple, building a list of 100 numbers and returning a list of strings
    //based on the modulus results in the branch
    public List<String> runQuestion1() {

        List<Integer> numbers = IntStream.rangeClosed(1, 100)
                .boxed().collect(Collectors.toList());

        return numbers.stream().map(number -> {
            if (number % 4 == 0 && number % 5 == 0) {
                return "HelloWorld";
            } else if (number % 4 == 0){
                return "Hello";
            } else if (number % 5 == 0) {
                return "World";
            } else {
                return number.toString();
            }
        }).collect(Collectors.toList());
    }

    //joining lists is not super efficient we would typically use hybernate to do a SQL join here
    //given the wording of the question I figured you wanted to see this
    //we're essentially flattening the 2 lists for each result in the orders stream
    //filtering that down to just the matching OrderProduct-Product and the current index then
    //calling the member function joinProduct to create a new OrderProduct instance with the linked objects and
    //collecting up all of the results into a new list
    public List<OrderProduct> runQuestion2(List<OrderProduct> orders) {

        List<Integer> productIds = orders.stream().map(order -> {
            return order.getProductId();
        }).collect(Collectors.toList());

        List<Product> products = productRepository.findByProductIdIn(productIds);

        List<OrderProduct> response = orders.stream()
            .flatMap(order -> products.stream()
            .filter(product -> Objects.equals(order.getProductId(), product.getProductId()))
            .map(product -> OrderProduct.joinProduct(order, product)))
            .collect(Collectors.toList());

        return response;
    }

    //There are lots of very sophisticated ways to solve scheduling problems. This is a super simple implementation.
    //We're building a list with every minute in the assumed 4 hour day and iterating over each minute to determine
    //the highest priority act at that point. The day is set to tomorrow Aus/Brissy time 8am to 12pm.
    public List<TimeSlot> runBonusQuestion(List<Performance> performances) {

        LocalDate today = LocalDate.now(Clock.system(ZoneId.of("Australia/Sydney"))).plusDays(1);
        LocalTime startingTime = LocalTime.of(8, 00);
        List<LocalDateTime> times = IntStream.rangeClosed(0, 240)
                .boxed()
                .map(minute -> {
                    return LocalDateTime.of(today,startingTime.plusMinutes(minute));
                }).collect(Collectors.toList());

        List<TimeSlot> schedule = times.stream().map(time -> {
            Performance bestPerformance = performances.stream()
                .filter(performance -> matchTimeToTimeSlot(time,performance.getStartTime(),performance.getFinishTime()))
                    .max(Performance::compareTo)
                    .orElse(Performance.create().setBand("Nothing Playing"));
            return TimeSlot.create().setBand(bestPerformance.getBand()).setWindow(time);
            })
            .collect(Collectors.toList());

        return schedule;
    }

    Boolean matchTimeToTimeSlot(LocalDateTime currentTime, LocalDateTime startTime, LocalDateTime endTime){
        return currentTime.equals(startTime) ||
                (currentTime.isAfter(startTime) &&
                    currentTime.isBefore(endTime)) ||
               currentTime.equals(endTime);
    }


}
