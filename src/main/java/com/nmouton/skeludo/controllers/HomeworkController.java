package com.nmouton.skeludo.controllers;

import com.nmouton.skeludo.entities.OrderProduct;
import com.nmouton.skeludo.entities.Performance;
import com.nmouton.skeludo.entities.TimeSlot;
import com.nmouton.skeludo.service.QuestionsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.*;
import java.util.Arrays;
import java.util.List;

@RestController
@RequestMapping(path="/api")
@CrossOrigin
public class HomeworkController {

    private final Logger logger = LoggerFactory.getLogger(HomeworkController.class);

    @Autowired
    QuestionsService questionsService;

    @GetMapping(path="/question1", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<List<String>> calculateAnswer1(){
        logger.info("received question1 request");
        List<String> answer1 = questionsService.runQuestion1();
        return new ResponseEntity<>(answer1, HttpStatus.ACCEPTED);
    }

    @PostMapping(path="/question2", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<List<OrderProduct>> calculateAnswer2(@RequestBody List<OrderProduct> orderProducts){
        logger.info("received question2 request: {}", orderProducts);

        List<OrderProduct> answer2 = questionsService.runQuestion2(orderProducts);
        return new ResponseEntity<>(answer2, HttpStatus.ACCEPTED);
    }

    @GetMapping(path="/bonusQuestion", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<List<TimeSlot>> calculateBonusAnswer(){
        logger.info("received bonus question request");
        LocalDate today = LocalDate.now(Clock.system(ZoneId.of("Australia/Sydney"))).plusDays(1);
        LocalTime startingTime = LocalTime.of(8, 00);
        LocalDateTime tomorrowAtEight = LocalDateTime.of(today,startingTime);

        //band 1 is 10 priority plays from 8 till 9
        Performance performance1 = Performance.create()
                        .setBand("band1")
                        .setPriority(10)
                        .setStartTime(tomorrowAtEight)
                        .setFinishTime(tomorrowAtEight.plusHours(1));
        //band 2 is 6 priority plays from 8 till 9
        Performance performance2 = Performance.create()
                .setBand("band2")
                .setPriority(6)
                .setStartTime(tomorrowAtEight.plusHours(1))
                .setFinishTime(tomorrowAtEight.plusHours(2));
        //band 3 is 8 priority plays from 10 till 11
        Performance performance3 = Performance.create()
                .setBand("band3")
                .setPriority(8)
                .setStartTime(tomorrowAtEight.plusHours(2))
                .setFinishTime(tomorrowAtEight.plusHours(3));
        //band 4 is 7 priority plays from 8:15 till 9:15
        Performance performanceClashesWith1And2 = Performance.create()
                .setBand("band4")
                .setPriority(7)
                .setStartTime(tomorrowAtEight.minusMinutes(15))
                .setFinishTime(tomorrowAtEight.plusHours(1).plusMinutes(15));
        //band 5 is 2 priority plays from 8 till 11
        Performance performanceClashesWithEverything = Performance.create()
                .setBand("band5")
                .setPriority(7)
                .setStartTime(tomorrowAtEight.minusMinutes(15))
                .setFinishTime(tomorrowAtEight.plusHours(1).plusMinutes(15));
        //list of bands
        List<Performance> performances = Arrays.asList(performance1,
                performance2,
                performance3,
                performanceClashesWith1And2,
                performanceClashesWithEverything);

        List<TimeSlot> bonusAnswer = questionsService.runBonusQuestion(performances);

        return new ResponseEntity<>(bonusAnswer, HttpStatus.ACCEPTED);
    }


}
