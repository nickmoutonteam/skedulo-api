package com.nmouton.skeludo.entities;

import java.time.LocalDateTime;

public class Performance {
    private String band;
    private LocalDateTime startTime;
    private LocalDateTime finishTime;
    private Integer priority;

    public static Performance create(){
        return new Performance();
    }

    public String getBand() {
        return band;
    }

    public Performance setBand(String band) {
        this.band = band;
        return this;
    }

    public LocalDateTime getStartTime() {
        return startTime;
    }

    public Performance setStartTime(LocalDateTime startTime) {
        this.startTime = startTime;
        return this;
    }

    public LocalDateTime getFinishTime() {
        return finishTime;
    }

    public Performance setFinishTime(LocalDateTime finishTime) {
        this.finishTime = finishTime;
        return this;
    }

    public Integer getPriority() {
        return priority;
    }

    public Performance setPriority(Integer priority) {
        this.priority = priority;
        return this;
    }

    @Override
    public String toString() {
        return "Performance{" +
                "band='" + band + '\'' +
                ", startTime=" + startTime +
                ", finishTime=" + finishTime +
                ", priority=" + priority +
                '}';
    }

    public int compareTo(Performance p)
    {
        return Integer.compare(priority, p.priority);
    }
}
