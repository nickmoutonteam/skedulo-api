package com.nmouton.skeludo.entities;

import com.fasterxml.jackson.annotation.JsonInclude;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "ORDER_PRODUCT")
public class OrderProduct{

    @Id
    private Integer orderId;
    private Integer productId;
    private String name;
    private Double price;

    public static OrderProduct create(){
        return new OrderProduct();
    }

    public Integer getOrderId() {
        return orderId;
    }

    public OrderProduct setOrderId(Integer orderId) {
        this.orderId = orderId;
        return this;
    }

    public Integer getProductId() {
        return productId;
    }

    public OrderProduct setProductId(Integer productId) {
        this.productId = productId;
        return this;
    }

    public String getName() {
        return name;
    }

    public OrderProduct setName(String name) {
        this.name = name;
        return this;
    }

    public Double getPrice() {
        return price;
    }

    public OrderProduct setPrice(Double price) {
        this.price = price;
        return this;
    }

    public static OrderProduct joinProduct(OrderProduct orderProduct, Product product) {
        OrderProduct joinedOrderProduct = new OrderProduct();
        joinedOrderProduct.setOrderId(orderProduct.getOrderId());
        joinedOrderProduct.setProductId(orderProduct.getProductId());
        joinedOrderProduct.setName(product.getName());
        joinedOrderProduct.setPrice(product.getPrice());

        return joinedOrderProduct;
    }

    @Override
    public String toString() {
        return "OrderProduct{" +
                "orderId=" + orderId +
                ", productId=" + productId +
                ", name='" + name + '\'' +
                ", price=" + price +
                '}';
    }
}
