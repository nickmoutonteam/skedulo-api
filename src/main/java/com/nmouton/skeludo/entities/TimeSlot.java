package com.nmouton.skeludo.entities;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;

import java.time.LocalDateTime;
import java.util.Comparator;
import java.util.List;

public class TimeSlot {

    @JsonDeserialize(using = LocalDateDeserializer.class)
    @JsonFormat(pattern="dd/MM/yyyy hh:mm")
    private LocalDateTime window;
    private String band;

    public static TimeSlot create() {
        return new TimeSlot();
    }

    public LocalDateTime getWindow() {
        return window;
    }

    public TimeSlot setWindow(LocalDateTime window) {
        this.window = window;
        return this;
    }

    public String getBand() {
        return band;
    }

    public TimeSlot setBand(String band) {
        this.band = band;
        return this;
    }

    @Override
    public String toString() {
        return "TimeSlot{" +
                "window=" + window +
                ", band='" + band + '\'' +
                '}';
    }

    public static TimeSlot fillTimeSlot(LocalDateTime window, List<Performance> performance) {
        performance.sort(new Comparator<Performance>() {
            @Override
            public int compare(Performance p1, Performance p2) {
                return p2.getPriority() - p1.getPriority();
            }
        });
        TimeSlot timeslot = TimeSlot.create()
                .setBand(performance.get(0).getBand())
                .setWindow(window);
        return timeslot;
    }
}
