create table product
(
   PRODUCT_ID integer,
   NAME varchar(255),
   PRICE double,
   primary key(PRODUCT_ID)
);
create table order_product
(
   ORDER_ID integer,
   PRODUCT_ID integer,
   NAME varchar(255),
   PRICE double
);